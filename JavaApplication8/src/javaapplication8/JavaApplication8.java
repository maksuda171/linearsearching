
package javaapplication8;

import java.util.Scanner;

public class JavaApplication8 
{

    public static void main(String[] args) 
    {
        int a[] = new int [6];
        
        Scanner sn = new Scanner(System.in);
        
        for(int i = 0; i < 6; i++) 
        {
            a[i] = sn.nextInt();
        }
        
        System.out.print("Enter the key value: ");
        int key = sn.nextInt();
        
        int i, flag = 0;
        for (i = 0; i < 6; i++) 
        {
            if(a[i] == key) 
            {
                flag = 1;
                break;
            } 
        }
        
        if(flag == 0)
            System.out.println("Value not found");
        else if(flag == 1)
            System.out.println(key + " is found in index " + i);
        
        
    }
    
}
